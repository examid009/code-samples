<?php

use App\Modules\Store\Http\Controllers\Web\StoreController;
use Illuminate\Support\Facades\Route;
use App\Modules\Store\Http\Controllers\Web\StoreApiFrontController;

Route::post('store', [StoreApiFrontController::class, 'getData'])
    ->name('store-list-data');

Route::post('store/simple_edit', [StoreApiFrontController::class, 'updateSimple'])
    ->name('store-simple-edit-action');

Route::post('store/edit', [StoreApiFrontController::class, 'updateFull'])
    ->name('store-edit-action');

Route::post('store/add', [StoreApiFrontController::class, 'addStore'])
    ->name('store-add-action');

Route::post('location/list', [StoreApiFrontController::class, 'getLocationsList'])
    ->name('location.list');

Route::post('store/delete', [StoreApiFrontController::class, 'deleteStore'])
    ->name('store-delete-action');
