<?php

use App\Modules\Store\Http\Controllers\Web\StoreController;
use Common\Modules\Company\Models\Company;
use Illuminate\Support\Facades\Route;

Route::prefix('company/api')
    ->middleware(['hasCompany:' . Company::SUPPLIER])
    ->group(base_path('/modules/Store/routes/frontend.php'));

Route::prefix('company')
    ->middleware(['hasCompany:' . Company::SUPPLIER])
    ->group(function() {
        Route::get('store', [StoreController::class, 'list'])
            ->name('store-list');
    });
