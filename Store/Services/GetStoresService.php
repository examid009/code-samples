<?php


namespace App\Modules\Store\Services;

use Common\Modules\Company\Repositories\StoreRepository;
use Common\Services\Utils\ArrayUtils;

class GetStoresService
{
    protected StoreRepository $storeRepository;

    public function __construct(StoreRepository $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    /**
     * Получение списка торговых точек
     * @param int $companyId
     * @param int $pageSize
     * @return array
     */
    public function getStores(int $companyId, int $pageSize, ?StoreFilterBuilder $builder = null)
    {
        $stores = $this->storeRepository->getSubjectStores($companyId);
        if (!empty($builder)) {
            $stores = $builder->build($stores);
        }
        $stores->withAggregate('cityName','name');
        $stores->orderBy('city_name_name', );
        $stores = $stores->paginate($pageSize)
            ->withQueryString();
        $stores = $stores->toArray();
        $stores['data'] = ArrayUtils::array_change_key_case_recursive($stores['data']);
        foreach ($stores['data'] as &$store) {
            $store['is_changing'] = false;
        }

        return $stores;
    }

    /**
     * Текстовка для статичного текста
     * @return array
     */
    public function getInterfaceTranslate()
    {
        return [
            'show_filter' => __('Показать фильтр'),
            'hide_filter' => __('Скрыть фильтр'),
            'add_poses_btn' => __('Добавить точку продаж'),
            'num_or_id' => __('№ или ID точки продаж'),
            'id_poses_text' => __('ID точки продаж'),
            'kato' => __('Като'),
            'nas_punkt' => __('Населённый пункт'),
            'address' => __('Адрес'),
            'self_export' => __('Самовывоз'),
            'self_delivery' => __('Самопривоз'),
            'activity' => __('Активность'),
            'edit' => __('Редактировать'),
            'delete' => __('Удалить'),
            'on_page' => __('На странице'),
            'back' => __('Назад'),
            'next' => __('Дальше'),
            'clear' => __('Очистить'),
            'submit' => __('Применить'),
            'yes' => __('Да'),
            'no' => __('Нет'),
            'delete_alert_title' => __('Удаление точки продаж'),
            'delete_alert_text' => __('Вы действительно хотите удалить точку продаж'),
            'add_store_title' => __('Добавить точку продаж'),
            'edit_store_title' => __('Редактировать точку продаж')
        ];
    }
}
