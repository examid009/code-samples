<?php

namespace App\Modules\Store\Services;

use Common\Modules\Company\Models\RefStreetType;
use Common\Services\Utils\ArrayUtils;

class EditStoreService
{
    public function getInterfaceTranslate()
    {
        return [
            'nasPunkt' => __('Населённый пункт'),
            'streetType' => __('Тип улицы'),
            'street' => __('Улица'),
            'home' => __('Дом'),
            'building' => __('Здание'),
            'apartment' => __('Квартира'),
            'self_delivery' => __('Точка самопривоза'),
            'self_export' => __('Самовывоз'),
            'street_name' => __('Название улицы'),
            'number' => __('Номер'),
            'add' => __('Добавить'),
            'required' => __('обязательные поля'),
            'close' => __('Закрыть'),
            'placeholder_nas_punkt' => __('Выбор города'),
            'placeholder_street_type' => __('Выберите тип улицы'),
            'placeholder_street' => __('Название улицы'),
            'placeholder_number' => __('Номер'),
            'add_store_btn' => __('Добавить'),
            'edit_store_btn' => __('Изменить')
        ];
    }

    public function getDictionaries()
    {
        $streetTypes = RefStreetType::query()
            ->get()
            ->toArray();

        $streetTypes = ArrayUtils::array_change_key_case_recursive($streetTypes);

        return [
            'streetTypes' => $streetTypes
        ];
    }
}
