<?php

namespace App\Modules\Store\Services;

use Illuminate\Database\Eloquent\Builder;

/**
 * Построитель запроса по фильтрам для Точек продаж
 */
class StoreFilterBuilder
{
    protected $filters = [];
    public function setFilters(array $filters): self
    {
        $this->filters = $filters;
        return $this;
    }
    public function build(Builder $builder): Builder
    {
        foreach ($this->filters as $key => $filter) {
            if (empty($filter)) {
                continue;
            }

            switch ($key) {
                case 'id':
                    $builder->where('id', $filter);
                    break;
                case 'region_id':
                    $builder->where('region_id', $filter);
                    break;
                case 'kato':
                    $builder->where('kato',$filter);
                    break;
                case 'self_export_available':
                    $builder->where('self_export_available', $filter);
                    break;
                case 'self_delivery_point':
                    $builder->where('self_delivery_point', $filter);
                    break;
                case 'active':
                    $builder->where('active', $filter);
                    break;
                default:
                    break;
            }
        }

        return $builder;
    }
}
