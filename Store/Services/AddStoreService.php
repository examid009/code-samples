<?php

namespace App\Modules\Store\Services;

use Common\Modules\Company\Repositories\LocationRepository;
use Common\Modules\Company\Repositories\StoreRepository;

class AddStoreService
{
    const DEFAULT_STREET_TYPE = 1;

    protected LocationRepository $locationRepository;
    protected StoreRepository $storeRepository;
    public function __construct(LocationRepository $locationRepository, StoreRepository $storeRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->storeRepository = $storeRepository;
    }
    public function handle(array $data, int $subjectId): bool
    {
        $element = $data;
        $element['subject_id'] = $subjectId;
        $element['active'] = true;
        $kato =  $this->locationRepository->getById($data['region_id']);
        if (empty($kato)) {
            throw new \Exception(__('КАТО не найдено'));
        }
        $element['kato'] = $kato->code;
        //Так как в базе зашито NOT NULL и при добавлении с точкой самопривоза давало создать точку и не меняя логику в базе
        if (empty($element['street_type_id'])) {
            $element['street_type_id'] = self::DEFAULT_STREET_TYPE;
        }

        $this->storeRepository->create($element);

        return true;
    }
}
