<?php

return [
    'enabled' => true,
    'views' => true,
    'job_start_seconds' => env('JOB_START_SECONDS', 3600)
];
