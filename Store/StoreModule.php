<?php

namespace App\Modules\Store;

use Oma\Core\Modules\Module;

class StoreModule extends Module
{

    public function rootDirectory(): string
    {
        return __DIR__;
    }

    public function providers(): array
    {
        return [
        ];
    }

    public function commands(): array
    {
        return [

        ];
    }
}
