<?php

namespace App\Modules\Store\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Store\Services\EditStoreService;
use App\Modules\Store\Services\GetStoresService;
use Common\Modules\Company\Models\Location;
use Common\Modules\Company\Models\Store;
use Common\Modules\Company\Repositories\CompanyRepository;
use Common\Modules\Company\Repositories\SelfPointRepository;
use Common\Services\Utils\ArrayUtils;
use Illuminate\View\View;

class StoreController extends Controller
{
    //Список точек продаж
    public function list(GetStoresService $storesService,
                         EditStoreService $editStoreService,
                         SelfPointRepository $selfPointRepository) : View
    {
        $selfDeliveryPoints = $selfPointRepository->getSelfPointsLocationsList();
        $points = array_column($selfDeliveryPoints->toArray(), 'loc_id');

        $selfDeliveryPoints = Location::query()
            ->with("name")
            ->whereIn('id',$points)
            ->get()
            ->toArray();
        $selfDeliveryPoints = ArrayUtils::array_change_key_case_recursive($selfDeliveryPoints);

        return view('store::pages.store.list', [
            'deleteStoreActionUrl' => route('store-delete-action'),
            'simpleStoreUpdateLink' => route('store-simple-edit-action'),
            'storeListUrl' => route('store-list-data'),
            'translate' => $storesService->getInterfaceTranslate(),
            'dictionaries' => $editStoreService->getDictionaries(),
            'translateModal' => $editStoreService->getInterfaceTranslate(),
            'selfDeliveryPoints' => $selfDeliveryPoints,
            'addAction' => route('store-add-action'),
            'editAction' => route('store-edit-action'),
        ]);
    }
}
