<?php

namespace App\Modules\Store\Http\Controllers\Web;

use App\Modules\Store\Http\Requests\AddStoreRequest;
use App\Modules\Store\Http\Requests\DeleteStoreRequest;
use App\Modules\Store\Http\Requests\EditStoreRequest;
use App\Modules\Store\Http\Requests\SimpleStoreUpdateRequest;
use App\Modules\Store\Services\AddStoreService;
use App\Modules\Store\Services\GetStoresService;
use App\Modules\Store\Services\StoreFilterBuilder;
use App\Services\PaginationService;
use Common\Modules\Company\Models\Store;
use Common\Modules\Company\Repositories\CompanyRepository;
use Common\Modules\Company\Repositories\LocationRepository;
use Common\Modules\Company\Repositories\SelfPointRepository;
use Common\Modules\Company\Repositories\StoreRepository;
use Common\Services\Utils\ArrayUtils;
use Illuminate\Http\Request;

class StoreApiFrontController
{
    //Добавление точки продаж (АПИ)
    public function addStore(AddStoreRequest $request,
                             AddStoreService $addStoreService,
                             CompanyRepository $companyRepository)
    {
        $companyId = $companyRepository->currentSubject()->id;
        $addStoreService->handle($request->all(), $companyId);

        return ['message' => __("Точка продаж добавлена")];
    }

    //Получение данных (АПИ)
    public function getData(
        CompanyRepository $companyRepository,
        GetStoresService $storesService,
        PaginationService $paginationService,
        Request $request
    )
    {
        $company = $companyRepository->currentSubject();
        $pageSize = $request->get('page_size');
        $filters = $request->all()['filters'] ?? [];

        $filterBuilder = null;
        if (!empty($filters)) {
            $filterBuilder = new StoreFilterBuilder();
            $filterBuilder->setFilters($filters);
        }

        return $storesService->getStores($company->id, $paginationService->getPaginationSize($pageSize), $filterBuilder);
    }

    //Простое обновление с таблицы
    public function updateSimple(SimpleStoreUpdateRequest $request, StoreRepository $storeRepository)
    {
        $updates = [];
        if ($request->has('active')) {
            $updates['active'] = $request->get('active');
            if (!$request->get('active')) {
                $updates['job_start'] = now()->addSeconds(config('store.job_start_seconds'));
            } else {
                $updates['job_start'] = null;
            }
        }

        $storeRepository->update($request->get('id'), $updates);

        return true;
    }

    public function updateFull(EditStoreRequest $request, StoreRepository $storeRepository)
    {
        $storeRepository->update($request->get('id'), $request->all());

        return ['message' => __("Точка продаж обновлена")];
    }

    public function getLocationsList(Request $request,
                                     LocationRepository $locationRepository,
                                     SelfPointRepository $selfPointRepository,
                                     CompanyRepository $companyRepository)
    {
        $res = $locationRepository->findByQuery($request->get('query'));

        if ($request->get('only_self')) {
            $company = $companyRepository->currentSubject();
            $stores = Store::query()
                ->where('subject_id', $company->id)
                ->get()
                ->toArray();

            $stores = ArrayUtils::array_change_key_case_recursive($stores);
            $stores = array_unique(array_column($stores, 'region_id'));

            $res->whereIn('id', $stores);
        }

        return ArrayUtils::array_change_key_case_recursive($res->get()->toArray());
    }

    public function deleteStore(DeleteStoreRequest $request, StoreRepository $storeRepository)
    {
        return $storeRepository->delete($request->get('id'));
    }
}
