<?php

namespace App\Modules\Store\Http\Requests;

use Common\Modules\Company\Models\Store;
use Common\Modules\Company\Repositories\CompanyRepository;
use Illuminate\Validation\Rule;

class EditStoreRequest extends AddStoreRequest
{
    public function rules(): array
    {
        $companyRepository = new CompanyRepository();
        $rules = parent::rules();
        return array_merge($rules, [
            'id' => [Rule::exists(Store::class,'id')->where('subject_id', $companyRepository->currentSubject()->id)],
        ]);
    }
}
