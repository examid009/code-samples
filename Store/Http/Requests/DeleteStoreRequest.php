<?php

namespace App\Modules\Store\Http\Requests;

use Common\Modules\Company\Models\Location;
use Common\Modules\Company\Models\RefStreetType;
use Common\Modules\Company\Models\Store;
use Common\Modules\Company\Repositories\CompanyRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteStoreRequest extends FormRequest
{
    public function rules()
    {
        $companyRepository = new CompanyRepository();
        return [
            'id' => ['required', Rule::exists(Store::class,'id')->where('subject_id', $companyRepository->currentSubject()->id)],
        ];
    }
}
