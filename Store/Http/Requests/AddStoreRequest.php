<?php

namespace App\Modules\Store\Http\Requests;

use Common\Modules\Company\Models\Location;
use Common\Modules\Company\Models\RefStreetType;
use Common\Modules\Company\Repositories\SelfPointRepository;
use Illuminate\Foundation\Http\FormRequest;

class AddStoreRequest extends FormRequest
{
    public function rules(): array
    {
        $rules = [
            'street_type_id'  => ['required_if:self_delivery_point,false'],
            'street_name'   => ['required_if:self_delivery_point,false','max:255'],
            'building' => ['required_if:self_delivery_point,false','max:255'],
            'self_export_available' => ['boolean'],
            'self_delivery_point' => ['boolean']
        ];

        $selfPointRepository = new SelfPointRepository();
        $rules['region_id'] = ['required', 'exists:'.Location::class.',id'];
        if ($this->get('self_export_available')) {
            $rules['region_id'][] = function ($attribute, $value, $fail) use ($selfPointRepository) {
                    $selfDeliveryPoints = $selfPointRepository->getSelfPointsLocationsList();
                    $points = array_column($selfDeliveryPoints->toArray(), 'loc_id');
                    if (!in_array($value, $points)) {
                        $fail(__('Выбранный регион недоступен для точки самопривоза'));
                    }
            };
        }

        return $rules;
    }

    public function messages():array
    {
        return [
            'region_id.required' => __('Не заполнено обязательное поле "Населённый пункт" '),
            'street_type_id.required_if' => __('Не заполнено обязательное поле "Тип улицы"'),
            'street_name.required_if' => __('Не заполнено обязательное поле "Улица"'),
            'building.required_if' => __('Не заполнено обязательное поле "Дом/Здание"')
        ];
    }
}
