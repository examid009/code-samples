<?php

namespace App\Modules\Store\Http\Requests;

use Common\Modules\Company\Models\Store;
use Common\Modules\Company\Repositories\CompanyRepository;
use Common\Modules\Company\Repositories\StoreRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SimpleStoreUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        $companyRepository = new CompanyRepository();
        return [
            'id' => ['required', Rule::exists(Store::class,'id')->where('subject_id', $companyRepository->currentSubject()->id)],
            'active' => ['required'],
        ];
    }

    public function messages():array
    {
        return [
            'id.required' => __('Не передан ID точки продаж'),
            'activity.required' => __('Переданы не все параметры'),
        ];
    }
}
