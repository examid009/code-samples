@extends('layouts.main')
@section('content')
    <div id="app"></div>

    @push('scripts')
        <script>
            window.__data = @js(Arr::except($__data, [
                '__env', '__path', 'app',
            ]))
        </script>
        @vite('modules/Store/js/pages/List/index.js')
    @endpush
@endsection


