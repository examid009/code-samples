@extends('layouts.main')
@section('content')
    <div class="mr-auto ml-auto w-1/2">
        <div class="flex justify-start items-center mb-5">
            <a href="{{ $backlink }}">
            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="40" height="40" rx="6" fill="#F2F2F2"/>
                <g clip-path="url(#clip0_199_42268)">
                    <path d="M22.0007 26L16.0007 19.5L22.0007 13" stroke="black" stroke-width="1.8" stroke-linecap="round"/>
                </g>
                <defs>
                    <clipPath id="clip0_199_42268">
                        <rect width="24" height="24" fill="white" transform="translate(8 8)"/>
                    </clipPath>
                </defs>
            </svg>
            </a>
            <div class="text-2xl font-semibold ml-5"> {{ $title }}</div>
        </div>
        <div id="app"></div>
    </div>


    @push('scripts')
        <script>
            window.__data = @js(Arr::except($__data, [
                '__env', '__path', 'app',
            ]))
        </script>
        @vite('modules/Store/js/pages/Add/index.js')
    @endpush
@endsection


