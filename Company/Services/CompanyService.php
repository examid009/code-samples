<?php

namespace App\Modules\Company\Services;

use Common\Modules\Company\Models\Company;
use Common\Modules\Company\Repositories\CompanyRepository;

class CompanyService
{
    public function __construct(
        protected CompanyRepository $companyRepository,
    )
    {

    }

    public function updateByBiin(string $biin, array $attributes)
    {
        $findCompany = $this->companyRepository->getByBiin($biin);

        return $findCompany->update($attributes);
    }
}
