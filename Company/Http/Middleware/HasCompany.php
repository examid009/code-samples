<?php

namespace App\Modules\Company\Http\Middleware;

use Common\Modules\Company\Models\Company;
use Common\Modules\Company\Repositories\CompanyRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HasCompany
{
    public function handle(Request $request, \Closure $next, ... $roles): Response
    {
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->currentSubject();

        if (empty($company)) {
            abort(403);
        }

        if (!empty($roles)) {
            if (in_array(Company::SUPPLIER ,$roles)) {
                if (!$company->isSupplier()) {
                    abort(403);
                }
            }
            if (in_array(Company::CUSTOMER, $roles)) {
                if (!$company->isCustomer()) {
                    abort(403);
                }
            }
        }

        return $next($request);
    }
}
