<?php

namespace App\Modules\Company\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CompanyController extends Controller
{
    public function list() : View
    {
        return view('store::pages.store.list', [
            'storeList' => [
                1 => 'г. Астана',
                2 => 'г. Алматы'
            ]
        ]);
    }
}
