<?php

namespace App\Modules\Company;

use Oma\Core\Modules\Module;

class CompanyModule extends Module
{

    public function rootDirectory(): string
    {
        return __DIR__;
    }

    public function providers(): array
    {
        return [
        ];
    }

    public function commands(): array
    {
        return [

        ];
    }

}
