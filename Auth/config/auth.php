<?php

return [
    'enabled' => true,
    'views' => true,
    'oauth' => [
        'url' => env('OAUTH_URL', 'http://localhost:8000'),
        'client_id' => env('OAUTH_CLIENT_ID', 'client-id'),
        'client_secret' => env('OAUTH_CLIENT_SECRET', 'client-secret'),
        'basic_login' => env('OAUTH_BASIC_LOGIN',''),
        'basic_password' => env('OAUTH_BASIC_PASSWORD','')
    ]
];
