<?php


namespace App\Modules\Auth\Providers;


use App\Modules\Auth\services\OAuth\GenericProviderExtended;
use Illuminate\Support\ServiceProvider;

class OAuthServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(GenericProviderExtended::class, function ($app) {
            $config = $app['config']->get('auth.oauth');

            return new GenericProviderExtended([
                'clientId' => $config['client_id'],
                'clientSecret' => $config['client_secret'],
                'redirectUri' => route('oauth-callback'),
                'urlAuthorize'            => $config['url'] . '/oauth/authorize/',
                'urlAccessToken'          => $config['url'] . '/oauth/access_token/'
            ]);
        });
    }
}
