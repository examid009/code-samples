<?php

namespace App\Modules\Auth\Http\Controllers\Web;

use Illuminate\Contracts\View\View;

class LoginController
{

    public function code(): View
    {
        return view('auth::pages.login.code', [
            'hello' => 'world'
        ]);
    }

}
