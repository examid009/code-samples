<?php


namespace App\Modules\Auth\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Auth\services\OAuth\GenericProviderExtended;
use App\Modules\Auth\services\OAuth\GetUserService;
use Common\Modules\Auth\Models\User;
use Illuminate\Http\Request;
use \Common\Modules\Company\Models\Company;

class OauthController extends Controller
{
    public function redirect(GenericProviderExtended $provider, Request $request)
    {
        $url = $provider->getAuthorizationUrl();

        session()->forget('oauth2state');
        session()->put('oauth2state', $provider->getState());

        return redirect($url);
    }

    public function callback(Request $request, GetUserService $service)
    {
        if (!$request->has('code') || !session()->has('oauth2state')) {
            return null;
        }

        if (session('oauth2state') !== $request->get('state')) {
            return null;
        }

        $user = $service->handle($request->get('code'));

        if (empty($user)) {
            return null;
        }

        foreach ($service->getData()['companies'] as $company) {
            if ($company['selected']) {
                $company = Company::query()
                    ->where('id', $company['id'])
                    ->first();

                if (empty($company)) {
                    return null;
                }

                session()->put('company', $company);
            }
        }

        auth()->login($user);

        return redirect()->route('home');
    }
}
