<x-layout title="">
    <div id="app"></div>

    @push('scripts')
        <script>
            window.__data = @js(Arr::except($__data, [
                '__env', '__path', 'app',
            ]))
        </script>
        @vite('modules/Auth/js/pages/Login/Code/index.js')
    @endpush
</x-layout>
