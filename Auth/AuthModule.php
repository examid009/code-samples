<?php

namespace App\Modules\Auth;

use App\Modules\Auth\Providers\OAuthServiceProvider;
use Oma\Core\Modules\Module;

class AuthModule extends Module
{

    public function rootDirectory(): string
    {
        return __DIR__;
    }

    public function providers(): array
    {
        return [
            OAuthServiceProvider::class
        ];
    }

    public function commands(): array
    {
        return [

        ];
    }

}
