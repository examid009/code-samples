<?php


namespace App\Modules\Auth\services\OAuth;


use Common\Modules\Auth\Models\User;

class GetUserService
{
    protected GenericProviderExtended $provider;
    protected array $data;

    public function __construct(GenericProviderExtended $provider)
    {
        $this->provider = $provider;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function handle(string $code): ?User
    {
        try {
            $token = $this->provider->getAccessToken('authorization_code', [
                'code' => $code,
            ]);
        } catch (\Throwable $e) {
            return null;
        }


        /** @var array $data */
        $data = $this->provider->getResourceOwner($token);
        $this->data = $data;

        if (empty($data)) {
            return null;
        }

        return User::query()
            ->where('id', $data['id'])
            ->first();
    }
}
