<?php


namespace App\Modules\Auth\services\OAuth;


use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;

class GenericProviderExtended extends GenericProvider
{
    protected function getDefaultHeaders()
    {
        $username = config('auth.oauth.basic_login') ?? '';
        $password = config('auth.oauth.basic_password') ?? '';

        // Кодируйте имя пользователя и пароль
        $credentials = base64_encode($username . ':' . $password);

        return [
            'Authorization' => 'Basic ' . $credentials,
        ];
    }

    public function getResourceOwnerDetailsUrl(AccessToken $token): string
    {
        return config('auth.oauth.url') . '/oauth/me/?' . http_build_query([
                'token' => $token->getToken(),
            ]);
    }

    protected function getRequiredOptions()
    {
        return [
            'urlAuthorize',
            'urlAccessToken'
        ];
    }

    protected function getAuthorizationHeaders($token = null)
    {
        return [];
    }

    protected function createResourceOwner(array $response, AccessToken $token): array
    {
        return $response['data'] ?? [];
    }
}
