<?php

use App\Modules\Auth\Http\Controllers\Web\LoginController;
use Illuminate\Support\Facades\Route;
use App\Modules\Auth\Http\Controllers\OauthController;

Route::get('login/code', [LoginController::class, 'code']);

Route::withoutMiddleware('auth')->group(function () {
    Route::get('oauth/redirect',[OauthController::class, 'redirect'])
        ->name('oauth-redirect');
    Route::get('oauth/callback',[OauthController::class, 'callback'])
        ->name('oauth-callback');

    if (!app()->isProduction()) {
        Route::get('test_auth', function () {
            $user = \Common\Modules\Auth\Models\User::query()
                ->where('ID', 4797)
                ->first();

            $company = \Common\Modules\Company\Models\Company::query()
                ->where('ID', 1898)
                ->first();

            auth()->login($user);
            session()->put('company', $company);
        });
    }
});

Route::get('logout', function () {
   auth()->logout();
   return redirect(config('services.redirect.url'));
})->name('auth.logout');


